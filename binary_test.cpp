#include <iostream>
#include "binarytree.h"

using namespace std;

//void print(char c)
// {
//    cout << c;
// }
int main()
{
    //       -
    // a            *
    //          b       c
    auto pa = new BiTNode<char>{'a', nullptr, nullptr};
    auto pb = new BiTNode<char>{'b', nullptr, nullptr};
    auto pc = new BiTNode<char>{'c', nullptr, nullptr};
    auto pm = new BiTNode<char>{'*', pb, pc};
    auto ps = new BiTNode<char>{'-', pa, pm};

    auto T = ps;
    auto print = [](char c) { cout << c; };
    
    ///先序遍历
    Preorder(T, print);
    cout << endl;
    ///中序遍历
    Inorder(T, print);
    cout << endl;
    ///后序遍历
    Postorder(T, print);
    cout << endl;

    //打印二叉树
    Print(T, '=', 0);
    //节点数，叶子节点数，高度
    cout << "NodeCount:" << NodeCount(T) << endl;
    cout << "LeafCount:" << LeafCount(T) << endl;
    cout << "Depth:" << Depth(T) << endl;


    return 0;
}