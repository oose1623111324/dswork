#include<iostream>
#include"sqstack.h"


int main()
{
    SqStack<int,8> S;
    InitStack(S);
    Push( S,10);
    Push( S,20);
    Push( S,30);
    Push( S,40);

    while (! StackEmpty(S))
    {
        int e;
        Pop(S,e);
        std::cout << e << std::endl;
        
      
    }
    return 0;
          
}