#pragma once

////二叉树类型定义

///二叉链表节点结构
template <typename E>
struct BiTNode
{
    E data;
    BiTNode *lchild, *rchild;
};

//二叉树
template <typename E>
using BiTree = BiTNode<E> *;

/////二叉树算法

//
////现需遍历二叉树 Preorder(T,visit)
template <typename E, typename F>
void Preorder(BiTree<E> T, F visit)
{
    if (T)
    {
        visit(T->data);
        Preorder(T->lchild, visit);
        Preorder(T->rchild, visit);
    }
}

///中序遍历二叉树 Inorder(T,visit)
template <typename E, typename F>
void Inorder(BiTree<E> T, F visit)
{
    if (T)
    {
        Inorder(T->lchild, visit);
        visit(T->data);
        Inorder(T->rchild, visit);
    }
}

///后序遍历二叉树 Postorder(T,visit)
template <typename E, typename F>
void Postorder(BiTree<E> T, F visit)
{
    if (T)
    {
        Postorder(T->lchild, visit);
        Postorder(T->rchild, visit);
        visit(T->data);
    }
}

//求二叉树节点数
template <typename E>
int NodeCount(BiTree<E> T)
{
    if (!T) return 0;
    else {
        auto L = NodeCount(T->lchild);
        auto R = NodeCount(T->rchild);
        return L + R + 1;
    }
}

///求二叉树叶子节点数
template <typename E>
int LeafCount(BiTree<E> T)
{
    if (!T) return 0;
    if (!T->lchild && !T->rchild) return 1;
    else {
        auto L = LeafCount(T->lchild);
        auto R = LeafCount(T->rchild);
        return L + R;
    }
}

///求二叉树的深度
template <typename E>
int Depth(BiTree<E> T)
{
    if (!T) return 0;
    else{
        auto L = Depth(T->lchild);
        auto R = Depth(T->rchild);
        return L > R ? L + 1 :R + 1;
    }
}

#include <iostream>
using namespace std;
///打印二叉树
template <typename E>
void Print(BiTree<E> T, char prefix = '=', int level = 0)
{
    if (T){
        Print(T->rchild, '/', level + 1);
        for (int i = 0; i < level; ++i)
            cout << "  ";
        cout << prefix << T->data << endl;
        Print(T->lchild, '\\', level + 1);
    }
}